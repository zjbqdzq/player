import sqlite3 from 'sqlite3'
import {join} from 'path'
import {homedir} from 'os'
const home = process.env.HOME || homedir()
const isDev = process.env.NODE_ENV !== 'production'

const database = new sqlite3.Database(isDev ? 'player.db' : join(home, 'player.db'), (err) => {
    if (err) {
        throw new Error(err)
    }
})

export function select(sql, params = []) {
    return new Promise((resolve, reject) => {
        database.all(sql, params, (err, rows) => {
            if (err) {
                console.warn(err, sql)
                reject(err)
            } else {
                console.debug(sql, params, rows)
                resolve(rows)
            }
        })
    })
}

export function selectOne(sql, params = []) {
    return new Promise((resolve, reject) => {
        database.get(sql, params, (err, row) => {
            if (err) {
                console.warn(err, sql)
                reject(err)
            } else {
                console.debug(sql, params, row)
                resolve(row)
            }
        })
    })
}

export function modify(sql, params = []) {
    return new Promise((resolve, reject) => {
        database.run(sql, params, (err) => {
            if (err) {
                console.error(err, sql)
                reject(err)
            } else {
                console.debug(sql, params)
                resolve()
            }
        })
    })
}

