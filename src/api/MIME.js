const MIME = {
    // 音频
    'mp3': 'audio/mpeg',
    'flac': 'audio/flac',
    'ogg': 'audio/ogg',
    'm4a': 'audio/x-m4a',
    'mid': 'audio/midi',
    'mp4a': 'audio/mp4',
    'wav': 'audio/wav',
    'wma': 'audio/x-ms-wma',
    // 视频
    'avi': 'video/x-msvideo',
    'dv': 'video/x-dv',
    'mp4': 'video/mp4',
    'mpeg': 'video/mpeg',
    'mpg': 'video/mpeg',
    'mov': 'video/quicktime',
    'wm': 'video/x-ms-wmv',
    'flv': 'video/x-flv',
    'mkv': 'video/x-matroska'
}
export function getMediaType(ext) {
    return MIME[ext]
}

export function canPlayAudio() {
    const arr = []
    for (let mimeKey in MIME) {
        if (Object.prototype.hasOwnProperty.call(MIME, mimeKey) && MIME[mimeKey].startsWith('audio'))
            arr.push(mimeKey)
    }
    return arr
}
