import {select, modify, selectOne} from "@/api/db"
import {v4} from "uuid";

const columns = ['id', 'musicId', 'playTime', 'deleted']

export function createTable() {
    return modify('create table if not exists play_record(id text primary key, musicId text, playTime text, deleted int default 0)')
}

export function insert(obj) {
    obj.$id = v4()
    return modify(`insert into play_record(${columns.join(', ')}) values(${columns.map(value => `$${value}`).join(', ')})`, obj)
}

export function update(obj) {
    return modify(`update play_record set ${columns.map(value => `${value} = $${value}`)} where id = $id`, obj)
}

export function del(id) {
    return modify(`update play_record set deleted = 1 where id = ?`, [id])
}

export function selectAll() {
    return select(`select * from play_record where deleted = 0 order by orderNum`)
}

export function getById(id) {
    return selectOne(`select * from play_record where deleted = 0 and id = ?`, [id])
}