import {select, modify} from "@/api/db"

const columns = ['id', 'dirId', 'orderNum', 'name', 'size', 'type', 'realPath', 'deleted']

export function createTable() {
    return modify('create table if not exists music_list(id text primary key, dirId text, orderNum integer, name text, size int, type text, realPath text, deleted int default 0)')
}

export function insert(obj) {
    return select(`select count(1) flag from music_list where realPath = ? and deleted = 1`, [obj.$realPath]).then(res => {
        if (res[0].flag) {
            return modify(`update music_list set dirId = ?, deleted = 0 where realPath = ? and deleted = 1`, [obj.$dirId, obj.$realPath])
        } else {
            return modify(`insert into music_list(${columns.join(', ')}) values(${columns.map(value => `$${value}`).join(', ')})`, obj)
        }
    })
}

export function update(obj) {
    return modify(`update music_list set ${columns.map(value => `${value} = $${value}`)} where id = $id`, obj)
}

export function del(id) {
    return modify(`update music_list set deleted = 1 where id = ?`, [id])
}

export function selectAll() {
    return select(`select * from music_list where deleted = 0 order by orderNum`)
}

export function selectByDirId(dirId) {
    return select(`select ml.*, count(pr.id) playTimes
                   from music_list ml
                            left join play_record pr on ml.id = pr.musicId and pr.deleted = 0
                   where ml.deleted = 0
                     and ml.dirId = ?
                   group by ml.id
                   order by ml.orderNum`, [dirId])
}

export function clear() {
    return modify(`update music_list set deleted = 1`)
}