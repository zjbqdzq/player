import {select, modify, selectOne} from "@/api/db"

const columns = ['id', 'parentId', 'name', 'realPath', 'deleted']

export function createTable() {
    return modify('create table if not exists dir_tree(id text primary key, parentId text, name text, realPath text, deleted int default 0)')
}

export function insert(obj) {
    return select(`select count(1) flag from dir_tree where realPath = ? and deleted = 1`, [obj.$realPath]).then(res => {
        if (res[0].flag) {
            return modify(`update dir_tree set id = ?, parentId = ?, deleted = 0 where realPath = ? and deleted = 1`, [obj.$id, obj.$parentId, obj.$realPath])
        } else {
            return modify(`insert into dir_tree(${columns.join(', ')}) values(${columns.map(value => `$${value}`).join(', ')})`, obj)
        }
    })
}

export function update(obj) {
    return modify(`update dir_tree set ${columns.map(value => `${value} = $${value}`)} where id = $id`, obj)
}

export function del(id) {
    return modify(`update dir_tree set deleted = 1 where id = ?`, [id])
}

export function selectAll() {
    return select(`select * from dir_tree where deleted = 0 order by orderNum`)
}

export function getById(id) {
    return selectOne(`select * from dir_tree where deleted = 0 and id = ?`, [id])
}

export function getDirTreeList() {
    return select(`select dt.*, count(ml.id) count from dir_tree dt left join music_list ml on dt.id = ml.dirId and ml.deleted = 0 where dt.deleted = 0 group by dt.id`).then(res => {
        function buildTree(parentId = 'root') {
            const subArr = res.filter(value => value.parentId === parentId);
            subArr.forEach(value => {
                value.children = buildTree(value.id)
                value.children.forEach(value1 => value.count += value1.count)

            })
            return subArr
        }
        return new Promise(resolve => {
            resolve(buildTree())
        })
    })
}

export function clear() {
    return modify(`update dir_tree set deleted = 1`)
}