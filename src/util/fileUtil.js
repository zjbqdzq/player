import {readdir, readdirSync, statSync} from "fs";
import {join, parse} from "path"
import {insert} from "@/api/dao/MusicListMapper";
import {insert as dirInsert} from '@/api/dao/DirTreeMapper'
import {v4 as uuidv4} from 'uuid'
import {canPlayAudio} from "@/api/MIME";

const canPlayAudioExt = canPlayAudio()
const cacheList = []

export async function readDir(dir, parentId = 'root', vueComponent = null) {
    const files = readdirSync(dir)
    console.log(files)
    let dirId
    if (files.length) {
        dirId = uuidv4()
        console.log(await dirInsert({
            $id: dirId,
            $parentId: parentId,
            $name: dir.substr(dir.lastIndexOf('\\') + 1),
            $realPath: dir,
            $deleted: 0
        }))
    }
    for (const fileName of files) {
        const index = files.indexOf(fileName);
        const realPath = join(dir, fileName)
        const {name, ext} = parse(realPath)
        const realExt = ext.substr(1)
        const stats = statSync(realPath)
        if (stats.isFile() && canPlayAudioExt.includes(realExt)) {
            const fileObj = {
                $id: uuidv4(),
                $orderNum: index,
                $dirId: dirId,
                $name: name,
                $size: stats.size,
                $type: realExt,
                $realPath: realPath,
                $deleted: 0
            }
            if (vueComponent) {
                vueComponent.totalItem += 1;
            }
            cacheList.push(fileObj)
        } else if (stats.isDirectory()) {
            await readDir(realPath, dirId, vueComponent)
        }
    }
}

export async function saveCache(vueComponent = null) {
    for (let cacheListElement of cacheList) {
        await insert(cacheListElement).then(() => {
            if (vueComponent) {
                vueComponent.completeItem += 1;
            }
        }).catch(() => {
            if (vueComponent) {
                vueComponent.completeItem += 1;
            }
        })
    }
    cacheList.splice(0);
}