import Vue from 'vue'
import App from './App.vue'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'

Vue.prototype.$isElectron = window && window.process && window.process.versions && window.process.versions['electron'];
Vue.config.productionTip = false
Vue.use(ElementUI)

const vue = new Vue({
  render: h => h(App),
})
vue.$mount('#app')
