'use strict'

import * as electron from 'electron'
import {app, BrowserWindow, protocol} from 'electron'
import {createProtocol} from 'vue-cli-plugin-electron-builder/lib'
import {canPlayAudio} from "@/api/MIME";

const isDevelopment = process.env.NODE_ENV !== 'production'
const menu = electron.Menu
// Scheme must be registered before the app is ready
protocol.registerSchemesAsPrivileged([
    {scheme: 'app', privileges: {secure: true, standard: true}}
])
let win

async function createWindow() {
    process.env.IS_ELECTRON = 'true'
    menu.setApplicationMenu(null)
    // Create the browser window.
    win = new BrowserWindow({
        title: 'player',
        width: 1024,
        height: 768,
        webContents: {
            openDevTools: true
        },
        webPreferences: {

            // Use pluginOptions.nodeIntegration, leave this alone
            // See nklayman.github.io/vue-cli-plugin-electron-builder/guide/security.html#node-integration for more info
            nodeIntegration: process.env.ELECTRON_NODE_INTEGRATION,
            contextIsolation: !process.env.ELECTRON_NODE_INTEGRATION
        }
    })

    const webPackDevServerUrl = process.env.WEBPACK_DEV_SERVER_URL;
    if (webPackDevServerUrl) {
        createProtocol('app')
        // Load the index.html when not in development
        win.loadURL(webPackDevServerUrl)
    } else {
        createProtocol('app')
        // Load the index.html when not in development
        win.loadURL('app://./index.html')
    }
}

// Quit when all windows are closed.
app.on('window-all-closed', () => {
    // On macOS it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit()
    }
})

const lock = app.requestSingleInstanceLock();
if (!lock) {
    app.quit()
} else {
    app.on('second-instance', () => {
        if (win) {
            if (win.isMinimized()) win.restore()
            win.focus()
        }
    })
    app.on('activate', () => {
        // On macOS it's common to re-create a window in the app when the
        // dock icon is clicked and there are no other windows open.
        if (BrowserWindow.getAllWindows().length === 0) createWindow()
    })
    app.on('ready', async () => {
        createWindow()
        if (isDevelopment) {
            win.webContents.openDevTools()
            electron.globalShortcut.register('Alt+D', () => {
                win.webContents.openDevTools()
            })
        }
    })
}


// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.


// Exit cleanly on request from parent process in development mode.
if (isDevelopment) {
    if (process.platform === 'win32') {
        process.on('message', (data) => {
            if (data === 'graceful-exit') {
                app.quit()
            }
        })
    } else {
        process.on('SIGTERM', () => {
            app.quit()
        })
    }
}
electron.ipcMain.on('openChooseFileDialog', (event) => {
    const ext = canPlayAudio()
    electron.dialog.showOpenDialog({
        title: '选尼玛的歌',
        filters: [{name: 'Audio', extensions: ext}],
        properties: ['openDirectory']
    }).then(res => {
        console.log(res)
        event.sender.send('openChooseFileDialog-reply', res)
    })
})
