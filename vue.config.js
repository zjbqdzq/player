module.exports = {
    publicPath: './',
    assetsDir: 'static',
    pluginOptions: {
        electronBuilder: {
            nodeIntegration: true,
            builderOptions: {
                nsis: {
                    oneClick: false,
                    allowToChangeInstallationDirectory: true,
                    perMachine: true
                }
            }
        }
    }
}
